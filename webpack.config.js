'use strict';

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const isProdBuild = process.env.NODE_ENV === 'production';

const plugins = [
	new webpack.DefinePlugin({
		IS_DEV_SERVER: 'IS_DEV_SERVER' in process.env
	})
];

const config = {
	context: path.join(__dirname, 'src'),
	entry: './main',
	output: {
		path: path.join(__dirname, 'dist'),
		filename: 'range-slider.js',
		library: 'rangeSlider',
		libraryTarget: 'umd'
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel',
				query: {
					presets: ['es2015'],
					plugins: ['transform-decorators-legacy']
				}
			}
		]
	}
};

if (!isProdBuild) {
	plugins.push(new HtmlWebpackPlugin());

	config.devtool = 'source-map';
} else {
	plugins.push(new webpack.optimize.UglifyJsPlugin({
		compress: {
			warnings: false
		}
	}));
}

config.plugins = plugins;

module.exports = config;