import { Context } from './Context';

export interface IElementArgs {
  child: Element;
}

export abstract class Element {
  child: Element = null;
  private _isDirty: boolean = true;

  constructor(args: IElementArgs) {
    this.child = args.child;
  }

  get shouldRePaint(): boolean {
    return this._isDirty;
  }

  setState(action: VoidFunction): void {
    try {
      action();
      this._isDirty = true;
    } catch (error) {
      // TODO: handle error
    }
  }

  abstract paint(context: Context): void;
}
