export interface IOptions {
  surface: ISurfaceOptions;
}

interface ISurfaceOptions {
  width: number;
  height: number;
  styles: CSSStyleDeclaration;
}
