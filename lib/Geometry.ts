export abstract class OffsetBase {
  protected _dx: number = null;
  protected _dy: number = null;

  constructor(dx: number, dy: number) {
    this._dx = dx;
    this._dy = dy;
  }
}

export class Size extends OffsetBase {
  constructor(width: number, height: number) {
    super(width, height);
  }

  get width(): number {
    return this._dx;
  }

  get height(): number {
    return this._dy;
  }
}
