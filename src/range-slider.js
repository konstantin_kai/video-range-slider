import {createCanvas, getCanvasCordinates} from './canvas'
import {Promise, on as rsvpOn} from 'rsvp'
import dispatcher from './dispatcher'
import * as constants from './constants'
import * as imageUtils from './image-utils'

import Rail from './rail'
import Slider from './slider'
import DataProcessor from './data-processor'
import ExtActionsProcessor from './ext-actions-processor'
import Loader from './loader'

let _linkToData;

export default class RangeSlider {
	constructor (container, options) {
		dispatcher.removeAllListeners();

		this.components = [];
		this.previosPosiotion = null;
		this._drawing = false;
		this._queue = null;
		this._isExt = false;

		const {width, height, styles} = options.canvas;

		const ctx = createCanvas(
			width,
			height,
			styles
		);

		options.ctx = ctx;
		options.container = container;
		options.root = this;

		this.options = options;

		this.init();
	}

	init () {
		this.options.container.appendChild(this.options.ctx.canvas);

		this._initListeners();
		this._registerExtCallbacks();

		const dataProcessor = new DataProcessor(this.options);
		const loader = new Loader(this.options);

		_linkToData = dataProcessor.data;

		this.components.push(loader);
		this.components.push(new Rail(this.options));
		this.components.push(new Slider(this.options));
		this.components.push(dataProcessor);

		this._extActionsProcessor = new ExtActionsProcessor(this.options);

		loader.render();
		this._initData();
	}

	render () {
		const initial = () => Promise.resolve(true);
		const {ctx} = this.options;

		if (this._drawing) return initial();

		ctx.save();

		ctx.clearRect(0, 0, this.options.canvas.width, this.options.canvas.height);

		this._drawing = true;
		const res = this.components.reduce((prev, current) => {
			const cRender = current.render;

			if (prev instanceof Promise) return prev.then(() => cRender.call(current));

			return cRender.call(current);
		}, initial());

		ctx.restore();

		return res.then(() => {
			this._drawing = false;
			dispatcher.emit(constants.APP_E_AFTER_RENDER);
		});
	}

	getData () {
		return _linkToData;
	}

	setData (params) {
		if (this._queue !== null) return this._queue.then(() => {
			this._queue = null;
			return this.setData(params);
		});

		const {image: url, timeLength} = params;
		let promise = Promise.resolve(true);

		if (url) {
			promise = promise.then(() => {
				return imageUtils.loadImage(imageUtils.createImage(), url).then(img => {
					return imageUtils.cutTimeline(img, this.options).then(url => {
						dispatcher.emit(constants.APP_E_SET_IMG, {url});
					});
				});
			});
		}

		if (timeLength) {
			promise = promise.then(() => {
				return dispatcher.emit(constants.APP_E_SET_LENGTH, {timeLength});
			});
		}

		this._queue = promise;

		return this._queue.then(() => {
			this.render()
		});
	}

	callMethod (method, params = {}) {
		this._isExt = true;

		const f = this._extActionsProcessor[method].bind(this._extActionsProcessor);

		if (typeof f === 'function') {
			return f(params);
		}

		return false;
	}

	_initData () {
		const {image = false, timeLength = false} = this.options.data;

		if (!image || !timeLength) return this;

		this.setData({image, timeLength});

		return this;
	}

	_initListeners () {
		const canvas = this.options.ctx.canvas;

		const _e = (e) => {
			const {s, t} = getCanvasCordinates(canvas);

			e.diffX = this.previosPosiotion ?
				Math.abs(this.previosPosiotion - e.pageX) :
				e.pageX;
			e.actualX = e.pageX - s;
			e.actualY = e.pageY - t;

			return e;
		}

		const listener = e => {
			const x = e.clientX;

			if (this.previosPosiotion === null) {
				return this.previosPosiotion = x;
			}

			if (this.previosPosiotion < x) dispatcher.emit(constants.APP_E_DRAG_RIGHT, _e(e));
			if (this.previosPosiotion > x) dispatcher.emit(constants.APP_E_DRAG_LEFT, _e(e));

			this.previosPosiotion = x;
		}

		canvas.addEventListener(constants.APP_E_MOUSEMOVE, e => {
			dispatcher.emit(constants.APP_E_DRAG, _e(e));
		});

		canvas.addEventListener(constants.APP_E_MOUSEDOWN, (e) => {
			dispatcher.emit(constants.APP_E_DRAG_START, _e(e));
			dispatcher.on(constants.APP_E_DRAG, listener);
		});

		canvas.addEventListener(constants.APP_E_MOUSEDBCLICK, e => {
			dispatcher.emit(constants.APP_E_MOUSEDBCLICK, _e(e));
		});

		document.addEventListener(constants.APP_E_MOUSEUP, () => {
			this.previosPosiotion = null;
			dispatcher.off(constants.APP_E_DRAG, listener);
			dispatcher.emit(constants.APP_E_DRAG_END);
		});

		rsvpOn('error', reason => console.error(reason));
	}

	_registerExtCallbacks () {
		const {callbacks} = this.options;

		const isFunc = (f) => typeof f === 'function';

		const runCb = (f, params) => {
			if (isFunc(f)) return f(params);

			return false;
		}

		dispatcher.on(constants.APP_E_DATA_PROCESSED, () => {
			return runCb(callbacks.onChanged, this.getData());
		});

		dispatcher.on(constants.APP_E_CURRENT_END_REACHED, () => {
			return runCb(callbacks.onCurrentEndReached);
		});

		dispatcher.on(constants.APP_E_EXTCB_CURRENT_CHANGED, params => {
			return runCb(callbacks.onCurrentChanged, params);
		});

		dispatcher.on(constants.APP_E_AFTER_RENDER, () => {
			return runCb(callbacks.afterRender);
		});
	}
}