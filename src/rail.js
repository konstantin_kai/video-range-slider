import BaseClass from './base-class'
import * as constants from './constants'
import ImageMixin from './image-mixin'
import dispatcher from './dispatcher'

@ImageMixin
export default class Rail extends BaseClass {
	constructor (options) {
		super(options);

		this.drawYPos = this.options.canvas.height - this.options.slider.height;

		this._initListeners();
	}

	_initListeners () {
		dispatcher.on(constants.APP_E_SET_IMG, this.setImage.bind(this));
	}

	_drawOverlay () {
		const c = this.ctx;
		const {height: railHeight, borderWidth} = this.options.slider

		c.beginPath();
		c.globalAlpha = this.options.canvas.overlayOpacity;
		c.fillStyle = this.options.canvas.overlayColor;
		c.fillRect(0, this.drawYPos + borderWidth / 2, this.options.canvas.width, railHeight - borderWidth * 2);
		c.globalAlpha = 1;

		return true;
	}

	render () {
		return this.drawImage().then(() => this._drawOverlay());
	}
}