import mixin from './mixin'

export default mixin({
	drawArcRectangle (x, y, width, height, radius, fill, stroke) {
		const ctx = this.ctx;
		let tr, rr, br, lr;

		tr = rr = br = lr = 0;

		if (typeof radius === 'object') {
			if ('topLeft' in radius) tr = radius.topLeft;
			if ('topRight' in radius) rr = radius.topRight;
			if ('bottomRight' in radius) br = radius.bottomRight;
			if ('bottomLeft' in radius) lr = radius.bottomLeft;
		} else {
			tr = rr = br = lr = radius;
		}

		stroke = !fill && typeof stroke === 'undefined' && true;

		ctx.beginPath();
		ctx.moveTo(x + tr, y);
		ctx.lineTo(x + width - rr, y);
		ctx.quadraticCurveTo(x + width, y, x + width, y + rr);
		ctx.lineTo(x + width, y + height - br);
		ctx.quadraticCurveTo(x + width, y + height, x + width - br, y + height);
		ctx.lineTo(x + lr, y + height);
		ctx.quadraticCurveTo(x, y + height, x, y + height - lr);
		ctx.lineTo(x, y + tr);
		ctx.quadraticCurveTo(x, y, x + tr, y);
		ctx.closePath();
		
		if (fill) return ctx.fill();
		if (stroke) return ctx.stroke();
	},

	drawTriangle (a, b, c, bgColor = 'black', borderColor) {
		const ctx = this.ctx;
		const {x: ax, y: ay} = a;
		const {x: bx, y: by} = b;
		const {x: cx, y: cy} = c;

		borderColor = borderColor || bgColor;

		ctx.beginPath()
		ctx.moveTo(ax, ay);
		ctx.lineTo(bx, by);
		ctx.lineTo(cx, cy);
		ctx.closePath();

		ctx.strokeStyle = borderColor;
		ctx.stroke();

		ctx.fillStyle = bgColor;
		ctx.fill();
	}
})