import BaseClass from './base-class'
import * as c from './constants'
import DrawLinesMixin from './draw-lines-mixin'
import RectMixin from './rect-mixin'
import dispatcher from './dispatcher'
import ShadowMixin from './shadow-mixin'

@ShadowMixin
@RectMixin
@DrawLinesMixin
export default class Tooltips extends BaseClass {
	constructor (options) {
		super(options);

		this._positionNames = ['start', 'end', 'current'];
		this._positions = {
			start: 0,
			end: this.options.slider.width,
			current: null
		};
		this._originals = {
			start: 0,
			end: this.options.slider.width,
			current: null
		}

		this._initListeners();
	}

	render () {
		return this._positionNames.forEach(point => {
			if (this._positions[point] !== null) {
				return this._drawTooltip(point)
					._drawLength(point)
					._drawTriangle(point);
			}
		}, this);
	}

	_isStart (x) {
		return x !== null && x <= 0;
	}

	_isEnd (x) {
		return x !== null && x + this.options.tooltip.width >= this.options.canvas.width;
	}

	_isOnCurrentTooltip (x, y) {
		const {current} = this._positions;
		const {width, height} = this.options.tooltip;

		return current !== null &&
			x > current &&
			x < current + width &&
			y > 0 && y < height;
	}

	_initListeners () {
		const {earWidth} = this.options.slider;
		const {width} = this.options.tooltip;

		dispatcher.on(c.APP_E_SLIDER_CHANGED, params => {
			this._originals.start = params.startPosX;
			this._originals.end = params.endPosX;

			this._positions.start = params.startPosX + earWidth / 2 - width / 2;
			this._positions.end = params.endPosX - earWidth / 2 - width / 2;

			if (params.current) {
				this._positions.current = params.current - width / 2;
				this._originals.current = params.current;
			}
		});

		dispatcher.on(c.APP_E_DATA_PROCESSED, params => this._lengthValues = params);

		dispatcher.on(c.APP_E_DRAG, e => {
			const {actualX: x, actualY: y} = e;

			if (this._isOnCurrentTooltip(x, y)) {
				return this.setCursorStyle(c.C_STYLE_MOVE);
			}

			return this.setCursorStyle(c.C_STYLE_DEF);
		});

		dispatcher.on(c.APP_E_DRAG_START, e => {
			const {actualX: x, actualY: y} = e;

			if (this._isOnCurrentTooltip(x, y)) {
				this._currentDragged = true;
			}

			dispatcher.once(c.APP_E_DRAG_END, () => this._currentDragged = false);
		});

		[c.APP_E_DRAG_LEFT, c.APP_E_DRAG_RIGHT].forEach(event => {
			dispatcher.on(event, e => {
				const {diffX} = e;

				if (this._currentDragged) {
					return dispatcher.emit(
						c.APP_E_CURRENT_CHANGED,
						{diffX: event === c.APP_E_DRAG_LEFT ? diffX * -1 : diffX}
					)
				}
			});
		});
	}

	_drawTooltip (type) {
		let point = this._positions[type];
		const ctx = this.ctx;
		const {width, height, bgColor, curBgColor} = this.options.tooltip;
		let {borderRadius} = this.options.tooltip;

		if (this._isStart(point)) point = 0;
		if (this._isEnd(point)) point = this.options.canvas.width - width;

		if (this._originals.current !== null && this._originals.current <= 1) {
			borderRadius = {
				topLeft: borderRadius,
				topRight: borderRadius,
				bottomRight: borderRadius,
				bottomLeft: 0 
			}
		}

		if (this._originals.current !== null && this._originals.current >= this.options.canvas.width) {
			borderRadius = {
				topLeft: borderRadius,
				topRight: borderRadius,
				bottomRight: 0,
				bottomLeft: borderRadius 
			}
		}

		ctx.fillStyle = type === 'current' ? curBgColor : bgColor;

		this._positions[type] = point;

		this.setShadow('0 0 3 rgba(0,0,0, .2)');
		this.drawArcRectangle(point, 0, width, height, borderRadius, true);
		this.resetShadow();

		return this;
	}

	_drawLength (type) {
		const ctx = this.ctx;
		const length = this._lengthValues[type];
		const {fontColor, fontSize, height, width, fontFamily} = this.options.tooltip;
		const posX = this._positions[type];
		const {lengthFormater} = this.options.callbacks;

		ctx.font = [fontSize, fontFamily].join('px ');
		ctx.fillStyle = fontColor;
		
		const text = typeof lengthFormater === 'function' ? lengthFormater(length) : length + 'sec';
		const textWidth = ctx.measureText(text).width;

		ctx.fillText(
			text,
			posX + width / 2 - textWidth / 2,
			height / 2 + parseFloat((fontSize / 3).toFixed())
		);

		return this;
	}

	_drawTriangle (type) {
		const point = this._positions[type];
		const oriPoint = this._originals[type];
		const {height, width, bgColor, curBgColor} = this.options.tooltip;
		const {earWidth} = this.options.slider;
		const halfX = point + width / 2;

		const coordinates = {
			a: {x: halfX - 5, y: height},
			b: {x: halfX + 5, y: height},
			c: {x: halfX, y: height + 5}
		}

		if (this._isStart(point)) {
			coordinates.c.x = type === 'current' ? oriPoint : oriPoint + earWidth / 2;
			coordinates.a.x = coordinates.c.x - 5;
			coordinates.b.x = coordinates.c.x + 5;
		}

		if (this._isEnd(point)) {
			coordinates.c.x = type === 'current' ? oriPoint : oriPoint - earWidth / 2;
			coordinates.a.x = coordinates.c.x + 5;
			coordinates.b.x = coordinates.c.x - 5;
		}

		this.drawTriangle(
			coordinates.a,
			coordinates.b,
			coordinates.c,
			type === 'current' ? curBgColor : bgColor
		);

		if (type === 'current') this._drawCurrentLine(oriPoint);

		return this;
	}

	_drawCurrentLine (x) {
		const {
			tooltip: {height, curBgColor},
			canvas: {height: canvasHeight},
			slider: {borderWidth}
		} = this.options;

		this.drawVerticalLine({
			x,
			y: height + 6,
			l: canvasHeight - borderWidth * 2 - height - 5,
			c: curBgColor,
			w: 1
		});
	}
}