import BaseClass from './base-class'
import ShadowMixin from './shadow-mixin'

@ShadowMixin
export default class Loader extends BaseClass {
	_drawLoader () {
		const ctx = this.ctx;

		const {
			canvas: {width, height},
			slider: {height: sliderHeight},
			loader: {fontSize, fontFamily, fontColor, text}
		} = this.options

		ctx.font = [fontSize, fontFamily].join('px ');
		ctx.fillStyle = fontColor;

		const textWidth = ctx.measureText(text).width;

		this.setShadow('0 3 3 rgba(0,0,0, .3)');
		ctx.fillText(
			text,
			width / 2 - textWidth / 2,
			height - sliderHeight / 2
		);
		this.resetShadow();
	}

	render () {
		return this._drawLoader();
	}
}
