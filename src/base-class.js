import * as c from './constants'

export default class BaseClass {
	constructor (options) {
		this.options = options;
		this.ctx = options.ctx;
		this.canvas = options.ctx.canvas;
		this.root = options.root;
	}

	setCursorStyle (style) {
		this.canvas.style.cursor = style;
	}

	getCanvasCordinates () {
		return {
			s: this.canvas.offsetLeft
		}
	}

	get isExt () {
		return this.root._isExt;
	}

	resetExt () {
		return this.root._isExt && (this.root._isExt = false);
	}
}