import mixin from './mixin'

function drawLine (params, type, ctx) {
	const {x, y, l, w, c} = params;
	
	ctx.lineWidth = w || 1;

	ctx.beginPath();
	ctx.moveTo(x, y);
	ctx.strokeStyle = c || 'black';

	switch (type) {
		case 'horizontal':
			ctx.lineTo(x + l, y);
		break;
		case 'vertical':
			ctx.lineTo(x, l + y);
		break;
	}
	
	ctx.stroke();
}

export default mixin({
	drawVerticalLine (params) {
		return drawLine(params, 'vertical', this.ctx);
	},

	drawHorizontalLine (params) {
		return drawLine(params, 'horizontal', this.ctx);
	}
});