import EventEmiter from 'wolfy87-eventemitter'

const dispatcher = new EventEmiter();

export default dispatcher;