import mixin from './mixin'

const shadowRegexp = /^(\d+)\s{1}(\d+)\s{1}(\d+)\s{1}([\s\S]+)$/

export default mixin({
	setShadow (shadow = '') {
		const ctx = this.ctx;
		const parsed = shadow.match(shadowRegexp);

		if (parsed === null) return false;

		const [match, offsetX, offsetY, blur, color] = parsed;

		ctx.shadowOffsetX = offsetX;
		ctx.shadowOffsetY = offsetY;
		ctx.shadowBlur = blur;
		ctx.shadowColor = color;

		return true;
	},

	resetShadow () {
		const ctx = this.ctx;

		ctx.shadowOffsetX = 0;
		ctx.shadowOffsetY = 0;
		ctx.shadowBlur = 0;
	}
});