import BaseClass from './base-class'
import * as c from './constants'
import dispatcher from './dispatcher'

export default class DataProcessor extends BaseClass {
	constructor (options) {
		super(options);

		this.data = {start: null, current: null, end: null, duration: null};

		this._initListeners();
	}

	render () {return this;}

	_initListeners () {
		const dispatch = (params) => {
			const currentBefore = this.data.current;

			dispatcher.emit(c.APP_E_DATA_PROCESSED, this._process(params));

			const current = this.data.current;
			
			if (!this.isExt && currentBefore !== null && currentBefore !== current) {
				dispatcher.emit(c.APP_E_EXTCB_CURRENT_CHANGED, {current});
			}

			this.resetExt();
		}

		dispatcher.on(c.APP_E_SLIDER_CHANGED, params => dispatch(params));

		dispatcher.on(c.APP_E_SET_LENGTH, params => {
			const {width} = this.options.canvas;

			this._setLength(params.timeLength);

			const _params = {
				startPosX: width * this.data.start / this._length,
				endPosX: width * this.data.end / this._length
			};

			if (this.data.current !== null) _params.current = width * this.data.current / this._length;

			return dispatch(_params);
		});

		dispatcher.on(c.APP_E_CURRENT_CHANGED_EXT, length => {
			const current = this.options.canvas.width * length / this._length;

			dispatcher.emit(c.APP_E_CURRENT_CHANGED, {current});
		});

		dispatcher.on(c.APP_E_EXT_DATA_CHANGED, params => {
			const {width} = this.options.canvas;
			const {start: s, end: e} = params;
			const data = {};

			if (typeof s !== 'undefined') data.start = width * s / this._length;
			if (typeof e !== 'undefined') data.end = width * e / this._length;

			dispatcher.emit(c.APP_E_DATA_CHANGED, data);
		});
	}

	_process (params) {
		const {startPosX, endPosX, current: curPosX} = params;
		const width = this.options.canvas.width;
		const length = this._length || 0;

		const start = Math.ceil(startPosX * length / width);
		const end = Math.floor(endPosX * length / width);

		this.data.start = start;
		this.data.end = end;
		this.data.duration = end - start;

		if (typeof curPosX !== 'undefined') {
			const current = Math.floor(curPosX * length / width);

			this.data.current = current;
		}
		
		return this.data;
	}

	_setLength (l) {
		this._length = l;

		return this;
	}
}