import BaseClass from './base-class'
import * as constants from './constants'
import ImageMixin from './image-mixin'
import Tooltips from './tooltips'
import DrawLinesMixin from './draw-lines-mixin'
import RectMixin from './rect-mixin'
import dispatcher from './dispatcher'

@ImageMixin
@DrawLinesMixin
@RectMixin
export default class Slider extends BaseClass {
	constructor (options) {
		super(options);

		const {
			canvas: {height: h}
		} = this.options;

		const {
			borderWidth,
			earWidth,
			width: initialSliderWidth,
			height: railHeight,
			enableCurrentOnStart
		} = this.options.slider;

		this.cpx = null;
		this.slx = 0;
		this.sly = borderWidth;
		this.slw = initialSliderWidth;
		this.ew = earWidth;
		this.current = enableCurrentOnStart ? 1 : null;

		this.drawYPos = h - railHeight;

		this._railDragged = false;
		this._leDragged = false;
		this._reDragged = false;
		this._freezed = false;

		this._tooltips = new Tooltips(this.options);

		this._initListeners();
	}

	resetImagePos () {
		this.dx = this._getImageXPos();

		return this;
	}

	render () {
		const {slx: startPosX, current} = this;

		dispatcher.emit(constants.APP_E_SLIDER_CHANGED, {
			startPosX,
			endPosX: this.slx + this.slw,
			current
		});

		return this._drawImage().then(() => {
			this._drawRect();

			this._drawEar('left');
			this._drawEar('right');
			this._tooltips.render();
		});
	}

	_isOnRail (x, y) {
		if (typeof y === 'undefined') y = this.drawYPos + 1;

		return x > this.slx + this.ew &&
			x < this.slx + this.slw - this.ew &&
			y > this.drawYPos;
	}

	_isBetweenSlider (x) {
		return x > this.slx && x < this.slx + this.slw;
	}

	_isOnEar (x, y) {
		return this._isOnLeftEar(x, y) || this._isOnRightEar(x, y);
	}

	_isOnLeftEar (x, y) {
		return x > this.slx && x < this.slx + this.ew && y > this.drawYPos;
	}

	_isOnRightEar (x, y) {
		return x > this.slx + this.slw - this.ew && x < this.slx + this.slw && y > this.drawYPos;
	}

	_initListeners () {
		const render = () => this.root.render();
		const emitStartChanged = () => {
			return dispatcher.emit(constants.APP_E_SLIDER_START_CHANGED, {startPosX: this.slx});
		};

		dispatcher.on(constants.APP_E_EXT_DURATION_FREEZE, () => {
			this._freezed = true;
			return this;
		});
		dispatcher.on(constants.APP_E_EXT_DURATION_UNFREEZE, () => {
			this._freezed = false;
			return this;
		});

		dispatcher.on(constants.APP_E_SET_IMG, this.setImage.bind(this));

		dispatcher.on(constants.APP_E_DRAG, e => {
			const {actualX: x, actualY: y} = e;

			switch (true) {
				case this._freezed:
					return (this._isOnRail(x, y) || this._isOnEar(x, y))
						&& this.setCursorStyle(constants.C_STYLE_MOVE);
				case this._isOnRail(x, y):
					this.setCursorStyle(constants.C_STYLE_MOVE);
				break;
				case this._isOnEar(x, y):
					this.setCursorStyle(constants.C_STYLE_EWR);
				break;
				case !this._isBetweenSlider(x, y):
					this.setCursorStyle(constants.C_STYLE_DEF);
				break;
			}
		});

		dispatcher.on(constants.APP_E_DRAG_START, e => {
			const {actualX, actualY: y} = e;

			switch (true) {
				case this._freezed && (this._isOnRail(actualX, y) || this._isOnEar(actualX, y)):
				case this._isOnRail(actualX, y):
					this._railDragged = true;
				break;
				case this._isOnLeftEar(actualX, y):
					this._leDragged = true;
				break;
				case this._isOnRightEar(actualX, y):
					this._reDragged = true;
				break;
			}

			dispatcher.once(constants.APP_E_DRAG_END, () => {
				this._railDragged = false;
				this._leDragged = false;
				this._reDragged = false;
			})
		});

		dispatcher.on(constants.APP_E_DRAG_LEFT, e => {
			const {diffX} = e;
			const maxRightX = this.slx + this.ew * 2 + this.options.slider.minWidth;

			switch (true) {
				case this._railDragged &&
					this.slx > 0:
					this.slx -= diffX;

					if (this.slx < 0) this.slx = 0;

					if (this.current !== null) {
						this.current -= diffX;
						if (this.current < 0) this.current = 0;
					}

					this.dx = this._getImageXPos();

					emitStartChanged();
					render();
				break;
				case this._leDragged &&
					this.slx - diffX >= 0:
					this.slx -= diffX;

					if (this.slx < 0) {
						this.slx = 0;
					} else {
						this.slw += diffX;
					}

					this.dx = this._getImageXPos();

					emitStartChanged();
					render();
				break;
				case this._reDragged &&
					this.slx + this.slw - diffX > maxRightX:
					this.slw -= diffX;

					if (this.current !== null && this.slx + this.slw <= this.current) this.current = this.slx + this.slw;

					render();
				break;
			}
		});

		dispatcher.on(constants.APP_E_DRAG_RIGHT, e => {
			const {diffX} = e;
			const end = this.slx + this.slw;
			const maxRightX = this.slx + this.slw - this.ew * 2 - this.options.slider.minWidth;

			switch (true) {
				case this._railDragged &&
					end < this.canvas.width:
					this.slx += diffX;

					if (this.current !== null) {
						this.current += diffX;

						if (this.current > this.canvas.width) this.current = this.canvas.width;
					}

					if (this.slx + this.slw > this.canvas.width) this.slx = this.canvas.width - this.slw;

					this.dx = this._getImageXPos();

					emitStartChanged();
					render();
				break;
				case this._leDragged &&
					this.slx + diffX <= maxRightX:
					this.slw -= diffX;
					this.slx += diffX;

					if (this.current !== null && this.slx >= this.current) this.current = this.slx;

					this.dx = this._getImageXPos();

					emitStartChanged();
					render();
				break;
				case this._reDragged &&
					this.slx + this.slw < this.canvas.width:
					this.slw += diffX;

					if (this.slx + this.slw > this.canvas.width) this.slw = this.canvas.width - this.slx;

					render();
				break;
			}
		});

		dispatcher.on(constants.APP_E_MOUSEDBCLICK, e => {
			const {actualX: x} = e;

			if (this._isBetweenSlider(x)) {
				this.current = x;

				render();
			}
		});

		dispatcher.on(constants.APP_E_CURRENT_CHANGED, params => {
			const {diffX, current} = params;

			let _current = this.current;

			const emitEndReached = (current) => {
				if (_current !== current) {
					dispatcher.emit(constants.APP_E_CURRENT_END_REACHED);
					_current = current;
				}
			}

			if (typeof diffX !== 'undefined') this.current += diffX;
			if (typeof current !== 'undefined') this.current = current;

			if (this.current < this.slx) this.current = this.slx;
			if (this.current >= this.slx + this.slw) {
				this.current = this.slx + this.slw + 0.1;
				emitEndReached(this.current);
			}

			render();
		});

		const rdr = (diffX) => {
			this._reDragged = true;
			dispatcher.emit(constants.APP_E_DRAG_RIGHT, {diffX});
			this._reDragged = false;
		}

		const rdl = (diffX) => {
			this._reDragged = true;
			dispatcher.emit(constants.APP_E_DRAG_LEFT, {diffX});
			this._reDragged = false;
		}

		const ldl = (diffX) => {
			this._leDragged = true;
			dispatcher.emit(constants.APP_E_DRAG_LEFT, {diffX});
			this._leDragged = false;
		}

		const ldr = (diffX) => {
			this._leDragged = true;
			dispatcher.emit(constants.APP_E_DRAG_RIGHT, {diffX});
			this._leDragged = false;
		}

		dispatcher.on(constants.APP_E_EXT_END_ADD, (v = 1) => rdr(v));
		dispatcher.on(constants.APP_E_EXT_END_SUB, (v = 1) => rdl(v));
		dispatcher.on(constants.APP_E_EXT_START_SUB, (v = 1) => ldl(v));
		dispatcher.on(constants.APP_E_EXT_START_ADD, (v = 1) => ldr(v));
		dispatcher.on(constants.APP_E_EXT_DURATION_SUB, (v = 1) => rdl(v));
		dispatcher.on(constants.APP_E_EXT_DURATION_ADD, (v = 1) => {
			const {width} = this.options.canvas;
			const diffX = v;

			if (this.slx + this.slw >= width) {
				return ldl(diffX);
			}
			
			return rdr(diffX);
		});

		dispatcher.on(constants.APP_E_DATA_CHANGED, params => {
			const {start = this.slx, end = this.slx + this.slw} = params;

			this.slx = this.current = start;
			this.slw = end - this.slx;
			this.dx = this._getImageXPos();

			render();
		});
	}

	_drawRect () {
		const {borderWidth, height, color, earWidth: ew} = this.options.slider;

		this.drawHorizontalLine({
			x: this.slx + ew,
			y: this.drawYPos,
			l: this.slw - ew * 2,
			w: borderWidth,
			c: color
		});
		this.drawHorizontalLine({
			x: this.slx + ew,
			y: this.drawYPos + height - borderWidth,
			l: this.slw - ew * 2,
			w: borderWidth,
			c: color
		});
	}

	_drawImage () {
		const {borderWidth, earWidth, height} = this.options.slider;

		const params = {
			width: this.slw - earWidth,
			height: height - borderWidth * 2,
			xPos: this._getImageXPos()
		};

		return this.drawImage(params);
	}

	_drawEar (direction) {
		const c = this.ctx;
		const ew = this.ew;
		const {height, borderWidth, borderRadius} = this.options.slider;
		const lineYPos = this.drawYPos + height / 3;
		const y = this.drawYPos - borderWidth / 2;

		const drawLine = (x, y, length) => {
			this.drawVerticalLine({
				x, y, l: length, c: this.options.slider.earDragColor
			})
		}

		c.beginPath();
		c.fillStyle = this.options.slider.color;

		switch (direction) {
			case 'left':
				this.drawArcRectangle(this.slx, y, ew, height, {
					topLeft: borderRadius,
					bottomLeft: borderRadius
				}, true);

				drawLine(this.slx + ew / 2 - 2, lineYPos, height / 3);
				drawLine(this.slx + ew / 2 + 1, lineYPos, height / 3)
			break;
			case 'right':
				this.drawArcRectangle(this.slx + this.slw - ew, y, ew, height, {
					topRight: borderRadius,
					bottomRight: borderRadius
				}, true);

				drawLine(this.slx + this.slw - ew / 2 + 2, lineYPos, height / 3);
				drawLine(this.slx + this.slw - ew / 2 - 1, lineYPos, height / 3);
			break;
		}
	}

	_getImageXPos () {
		return this.slx + this.options.slider.earWidth;
	}
}