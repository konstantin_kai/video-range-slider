import mixin from './mixin'
import {Promise} from 'rsvp'
import {createImage, loadImage} from './image-utils'

export default mixin({
	getImage () {
		if (!this._image) {
			this._image = createImage();
		}

		return this._image;
	},

	resetImagePos () {
		this.dx = 0;

		return this;
	},

	setImage (params) {
		this.resetImagePos();
		this._imageSrc = params.url;

		return this._image && delete this._image._cutted;
	},

	drawImage (params = {}) {
		const c = this.ctx;
		const img = this.getImage();
		const {borderWidth, height: railHeight} = this.options.slider;

		const {width, height, xPos, imgWidth: iw} = params

		const w = width || this.options.canvas.width;
		const h = height || railHeight - borderWidth * 2;
		const imgWidth = iw || w;

		const drawImage = () => {
			if (this.dx + w > img.width) this.dx = 0;

			c.drawImage(
				img,
				this.dx || 0, 0,
				imgWidth, h,
				xPos || 0, this.drawYPos + borderWidth / 2,
				w, h
			);

			return true;
		}

		if (img.src !== this._imageSrc) {
			return loadImage(img, this._imageSrc).then(() => drawImage());
		}

		return Promise.resolve(drawImage());
	}
});