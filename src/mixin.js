export default function mixin (behaviour, sharedBehaviour = {}) {
	const instanceKeys = Reflect.ownKeys(behaviour);
	const sharedKeys = Reflect.ownKeys(sharedBehaviour);
	const typeTag = Symbol('isa');
	
	const _mixin = (Constructor) => {
		for (const property of instanceKeys) {
			if (!Constructor.prototype.hasOwnProperty(property)) {
				Object.defineProperty(Constructor.prototype, property, {value: behaviour[property]});
			}
		}
		
		Object.defineProperty(Constructor.prototype, typeTag, {value: true});
		
		return Constructor;
	}

	for(const property of sharedKeys) {
		Object.defineProperty(_mixin, property, {
			value: sharedBehaviour[property],
			enumerable: sharedBehaviour.propertyIsEnumerable(property)
		});
	}

	Object.defineProperty(_mixin, Symbol.hasInstance, {
		value: (i) => !!i[typeTag]
	});

	return _mixin;
}