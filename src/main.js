import RangeSlider from './range-slider'
import merge from 'merge'

import defOptions from './default-options';

function create (selector, options) {
	let element = selector || 'body';

	if (!(typeof element === 'object' && 'nodeType' in element && 'ownerDocument' in element)) {
		element = document.querySelector(selector);
	}


	if (!element) {
		throw new Error('Element for selector not found');
	}

	return new RangeSlider(element, merge.recursive({}, defOptions, options));
}

export default create;

if (IS_DEV_SERVER) {
	const div = document.createElement('div');
	div.setAttribute('id', 'container');

	const div2 = document.createElement('div');
	div2.style.height = '300px';

	div.appendChild(div2);

	div.style.position = 'relative';
	div.style.height = '600px';
	div.style.margin = '0 auto';
	div.style.maxWidth = '50%';
	div.style.backgroundColor = 'gray';

	document.body.appendChild(div);

	window._rs = create('#container');
}