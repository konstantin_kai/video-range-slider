const mainColor = '#899199';

const config = {
	canvas: {
		width: 620,
		height: 110,
		overlayColor: 'rgba(137, 145, 153, 0.7)',
		overlayOpacity: 0.7
	},
	slider: {
		width: 300,
		height: 70,
		color: mainColor,
		earDragColor: 'white',
		borderWidth: 4,
		borderRadius: 3,
		earWidth: 15,
		minWidth: 2,
		enableCurrentOnStart: true
	},
	tooltip: {
		width: 40,
		height: 20,
		bgColor: mainColor,
		curBgColor: '#6DB65B',
		fontSize: 12,
		fontColor: 'white',
		fontFamily: 'Arial',
		borderRadius: 3
	},
	data: {},
	timeline: {
		thumbWidth: 135,
		maxAmount: 6
	},
	loader: {
		text: 'Loading timeline...',
		fontSize: 18,
		fontColor: 'rgba(0,0,0, .8)',
		fontFamily: 'Arial',
	},
	callbacks: {
		onChanged: (params) => console.log('m: onChange', params),
		onCurrentEndReached: (params) => console.log('m: onCurrentEndReached', params),
		onCurrentChanged: (params) => console.log('m: onCurrentChanged', params),
		lengthFormater: (value) => value + ' s'
	}
};

if (IS_DEV_SERVER) {
	config.data = {
		timeLength: 120,
		image: require('file?name=[path][name].[ext]!./static/w135i.jpeg')
	};
}

export default config;