import BaseClass from './base-class'
import dispatcher from './dispatcher'
import * as c from './constants'

let slider;

export default class ExtActionsProcessor extends BaseClass {
	constructor (options) {
		super(options);

		slider = this.root.components[2];

		this._initListeners()
	}

	_initListeners () {
		document.addEventListener('keydown', e => {
			switch (e.keyCode) {
				case c.KEY_CODE_RIGHT:
					this.updateRange();
				break;
				case c.KEY_CODE_LEFT:
					this.updateRange({
						type: 'start',
						operation: 'sub'
					});
				break;
			}
		});
	}

	isDurationFreezed () {
		return slider._freezed;
	}

	freezeDuration () {
		return dispatcher.emit(c.APP_E_EXT_DURATION_FREEZE) && true;
	}

	unfreezeDuration () {
		return dispatcher.emit(c.APP_E_EXT_DURATION_UNFREEZE) && true;
	}

	updateRange (params = {}) {
		const {type: t = 'end', operation: o = 'add', value: v = 1} = params;

		if (this.isDurationFreezed()) return false;

		switch ([t, o].join('_')) {
			case 'end_add':
				dispatcher.emit(c.APP_E_EXT_END_ADD, v);
			break;
			case 'end_sub':
				dispatcher.emit(c.APP_E_EXT_END_SUB, v);
			break;
			case 'start_add':
				dispatcher.emit(c.APP_E_EXT_START_ADD, v);
			break;
			case 'start_sub':
				dispatcher.emit(c.APP_E_EXT_START_SUB, v);
			break;
			case 'duration_add':
				dispatcher.emit(c.APP_E_EXT_DURATION_ADD, v);
			break;
			case 'duration_sub':
				dispatcher.emit(c.APP_E_EXT_DURATION_SUB, v);
			break;
		}
	}

	updateCurrent (params) {
		const {length} = params;

		if (typeof parseInt(length) === 'number') {
			return dispatcher.emit(c.APP_E_CURRENT_CHANGED_EXT, length) && true;
		}

		return false;
	}

	updateData (params) {
		return dispatcher.emit(c.APP_E_EXT_DATA_CHANGED, params) && true;
	}
}