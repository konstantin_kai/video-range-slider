export const createCanvas = (w, h, styles = {}) => {
	const canvas = document.createElement('canvas');

	canvas.width = w;
	canvas.height = h;

	for (const s in styles) {
		canvas.style[s] = styles[s];
	}

	return canvas.getContext('2d');
};

export const getCanvasCordinates = canvas => {
	const obj = canvas.getBoundingClientRect();

	return {
		s: obj.left,
		e: obj.right,
		t: obj.top
	};
};