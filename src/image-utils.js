import {createCanvas} from './canvas'
import {Promise} from 'rsvp'

export const createImage = () => {
	const img = new Image();

	img.setAttribute('crossOrigin', 'anonymous');

	return img;
}

export const loadImage = (img, url) => {
	const promise = new Promise((resolve, reject) => {
		img.onload = () => resolve(img);
		img.onerror = () => reject(img);
	});

	img.src = url;

	return promise;
}

export const cutTimeline = (img, options) => {
	const {
		timeline: {thumbWidth, maxAmount},
		canvas: {width, height},
		slider: {borderWidth}
	} = options;

	const imgWidth = img.width;
	const imgHeight = height - borderWidth * 2;

	const curAmount = Math.ceil(imgWidth / thumbWidth);
	const coordX = new Array(curAmount).join().split(',').map((v, idx) => {
		return (imgWidth / curAmount * (idx + 1)) - thumbWidth;
	});

	const pointer = Math.floor(coordX.length / (maxAmount - 2));
	const filteredX = coordX.filter((v, idx) => {
		if (idx === 0) return true;
		if (idx === coordX.length - 1) return true;

		return (idx + 1) % pointer === 0;
	});

	const ctx = createCanvas(filteredX.length * thumbWidth, imgHeight);

	filteredX.forEach((x, idx) => {
		ctx.drawImage(img, x, 0, thumbWidth, imgHeight, thumbWidth * idx, 0, thumbWidth, imgHeight);
	});

	const originalWidthCtx = createCanvas(width, imgHeight);
	const originalWidthImage = createImage();
	
	return loadImage(originalWidthImage, ctx.canvas.toDataURL())
		.then(img => {
			originalWidthCtx.drawImage(img, 0, 0, img.width, imgHeight, 0, 0, width, imgHeight);

			return originalWidthCtx.canvas.toDataURL();
		});
}