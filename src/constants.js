export const C_STYLE_MOVE = 'move';
export const C_STYLE_DEF = 'default';
export const C_STYLE_EWR = 'ew-resize';

export const KEY_CODE_RIGHT = 39;
export const KEY_CODE_LEFT = 37;

export const APP_E_SET_IMG = 'app_set_img';
export const APP_E_SET_LENGTH = 'app_set_length';

export const APP_E_MOUSEUP = 'mouseup';
export const APP_E_MOUSEDOWN = 'mousedown';
export const APP_E_MOUSEMOVE = 'mousemove';
export const APP_E_MOUSEDBCLICK = 'dblclick';
export const APP_E_DRAG_START = 'drag_start';
export const APP_E_DRAG_END = 'drag_end';
export const APP_E_DRAG = 'mouse_move';
export const APP_E_DRAG_LEFT = 'drag_left';
export const APP_E_DRAG_RIGHT = 'drag_right';
export const APP_E_SLIDER_CHANGED = 'app_slider_changed';
export const APP_E_DATA_PROCESSED = 'app_data_processed';
export const APP_E_DATA_CHANGED = 'app_data_changed';
export const APP_E_CURRENT_CHANGED = 'app_current_changed';
export const APP_E_CURRENT_CHANGED_EXT = 'app_current_changed_ext';
export const APP_E_CURRENT_REMOVED = 'app_current_removed';
export const APP_E_CURRENT_END_REACHED = 'app_current_end_reached';
export const APP_E_EXTCB_CURRENT_CHANGED = 'app_extcb_current_changed';
export const APP_E_SLIDER_START_CHANGED = 'app_slider_start_changed';
export const APP_E_AFTER_RENDER = 'app_after_render';

export const APP_E_EXT_START_ADD = 'app_ext_start_add';
export const APP_E_EXT_START_SUB = 'app_ext_start_sub';
export const APP_E_EXT_END_ADD = 'app_ext_end_add';
export const APP_E_EXT_END_SUB = 'app_ext_end_sub';
export const APP_E_EXT_DURATION_ADD = 'app_ext_duration_add';
export const APP_E_EXT_DURATION_SUB = 'app_ext_duration_sub';

export const APP_E_EXT_DURATION_FREEZE = 'app_ext_duration_freeze';
export const APP_E_EXT_DURATION_UNFREEZE = 'app_ext_duration_unfreeze';

export const APP_E_EXT_DATA_CHANGED = 'app_ext_data_changed';